// bank-rs ; An abstract bank-transaction parser.
// Copyright (C) 2017  Ruben De Smet
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use std;
use std::io;
use std::str::FromStr;

#[derive(Clone,Debug)]
pub struct Iban {
    iban: String,
}

impl FromStr for Iban {
    type Err = io::Error;
    fn from_str(s: &str) -> io::Result<Iban> {
        Ok(Iban {
            iban: s.to_string(),
        })
    }
}

impl std::fmt::Display for Iban {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::result::Result<(), std::fmt::Error> {
        write!(f, "{}", self.iban)
    }
}
