// bank-rs ; An abstract bank-transaction parser.
// Copyright (C) 2017  Ruben De Smet
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#[macro_use]
extern crate log;
extern crate csv;
extern crate chrono;
extern crate bigdecimal;
extern crate encoding;
extern crate num;

use std::io::{self, BufReader, Read};

use chrono::prelude::*;
use bigdecimal::BigDecimal;

mod banks;
mod iban;

/// "Structured creditor reference" module.
///
/// An implementation of the EPC142-08 standard, including an RF generator.
///
/// Additionally implements the OGM as the legacy Belgian standard for structured messages.
pub mod scr;

pub use iban::Iban;

pub trait Transaction {
    fn get_identifier(&self) -> &str;
    fn settlement_date(&self) -> Date<Utc>;
    fn amount(&self) -> &BigDecimal;
    fn currency(&self) -> &str;
    fn date(&self) -> Date<Utc>;
    fn origin_account(&self) -> &Iban;
    fn counterparty_account(&self) -> &Iban;
    fn counterparty_name(&self) -> &str;
    fn first_message(&self) -> &str;
    fn second_message(&self) -> &str;
}

pub trait Transactions<R: Read> {
    fn get_origin_account(&self) -> Option<::Iban> {
        None
    }
    fn read_transactions<'a>(&'a mut self) -> Box<Iterator<Item=Box<Transaction>> + 'a>;
}

pub trait Bank {
    fn get_name() -> &'static str;
    fn read_transaction_file<R: Read + 'static>(reader: BufReader<R>) -> io::Result<Box<Transactions<R>>>;
    fn recognise_transaction_file<R: Read + 'static>(reader: &mut BufReader<R>) -> bool;
}

use std::io::{Seek, SeekFrom};
macro_rules! detect_bank {
    ($bank:ident, $reader:ident) => (
        if banks::$bank::recognise_transaction_file(&mut $reader) {
            $reader.seek(SeekFrom::Start(0)).unwrap();
            return banks::$bank::read_transaction_file($reader);
        }
    )
}

pub fn read_transaction_file<R: Read + std::io::Seek + 'static>(mut reader: BufReader<R>) -> io::Result<Box<Transactions<R>>> {
    detect_bank!(Argenta, reader);
    Err(io::Error::new(io::ErrorKind::InvalidData, "Could not detect bank"))
}

struct GenericTransaction {
    id: String,
    settlement_date: Date<Utc>,
    description: String,
    amount: BigDecimal,
    date: Date<Utc>,
    counterparty_account: Iban,
    origin_account: Iban,
    counterparty_name: String,
    currency: String,
    message_1: String,
    message_2: String,
}

impl ::Transaction for GenericTransaction {
    fn get_identifier(&self) -> &str {
        &self.id
    }
    fn settlement_date(&self) -> Date<Utc> {
        self.settlement_date
    }
    fn amount(&self) -> &BigDecimal {
        &self.amount
    }
    fn currency(&self) -> &str {
        &self.currency
    }
    fn date(&self) -> Date<Utc> {
        self.date
    }
    fn origin_account(&self) -> &Iban {
        &self.origin_account
    }
    fn counterparty_account(&self) -> &Iban {
        &self.counterparty_account
    }
    fn counterparty_name(&self) -> &str {
        &self.counterparty_name
    }
    fn first_message(&self) -> &str {
        &self.message_1
    }
    fn second_message(&self) -> &str {
        &self.message_2
    }
}
